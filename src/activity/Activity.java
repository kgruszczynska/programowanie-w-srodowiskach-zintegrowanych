package activity;


public class Activity {
   public int ID;
   public String name;
   public String unit;
   public int quantity;
   
   public Activity(int ID, String name, String unit, int quantity) {
       this.ID = ID;
       this.name = name;
       this.unit = unit;
       this.quantity = quantity;
   }
   
   public Activity(String name, String unit, int quantity) {
       this.name = name;
       this.unit = unit;
       this.quantity = quantity;
   }
   
   public int getID() {
        return this.ID;
    }
   
   public String getName() {
       return this.name;
   }
   
   public String getUnit() {
       return this.unit;
   }
   
   public int getQuantity() {
       return this.quantity;
   }
   
   public void addQuantity(int quantity)
   {
       this.quantity += quantity;
   }
   
    public String formatToString() {
       return this.ID + " " + this.name + " " + this.unit + " " + this.quantity;
   }
    
    public String[] toArray()
    {
        return new String[] {Integer.toString(this.ID), this.name, this.unit, Integer.toString(this.quantity)};
    }
}

