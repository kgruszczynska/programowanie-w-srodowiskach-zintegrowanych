package activity;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;

public class Repository {

    private Connection conn;
    private Statement stat;
    public static final String DRIVER = "org.sqlite.JDBC";
    public static final String DB_URL = "jdbc:sqlite:database.db";

    public Repository() throws SQLException {
        try {
            Class.forName(Repository.DRIVER);
        } catch (ClassNotFoundException e) {
            System.err.println("Brak sterownika JDBC");
            e.printStackTrace();
        }
        try {
            this.conn = DriverManager.getConnection(Repository.DB_URL);
            this.stat = conn.createStatement();
        } catch (SQLException e) {
            System.err.println("Problem z otwarciem polaczenia");
            e.printStackTrace();
        }
    }

    public void closeConnection() {
        try {
            conn.close();
        } catch (SQLException e) {
            System.err.println("Problem z zamknieciem polaczenia");
            e.printStackTrace();
        }
    }

    public boolean insertActivity(Activity activity) {
        try {
            SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
            String date = ft.format(new java.util.Date());
            PreparedStatement prepStmt = conn.prepareStatement(
                    "INSERT INTO activity(name, unit, quantity, date_created) VALUES (?, ?, ?, ?)");
            prepStmt.setString(1, activity.getName());
            prepStmt.setString(2, activity.getUnit());
            prepStmt.setInt(3, activity.getQuantity());
            prepStmt.setString(4, date);
            prepStmt.execute();
        } catch (SQLException e) {
            System.err.println("Blad");
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public Activity selectById(int id) throws SQLException {
        int quantity = 0;
        String name = null, unit = null;
        try {
            ResultSet result = this.stat.executeQuery("SELECT * FROM activity WHERE ID = " + id);
            while (result.next()) {
                if (result == null) {
                    System.out.println("null dupa");
                    return null;
                } else {
                    id = result.getInt("ID");
                    name = result.getString("name");
                    unit = result.getString("unit");
                    quantity = result.getInt("quantity");
                }
            }
            Activity activity = new Activity(id, name, unit, quantity);

            return activity;
        } catch (SQLException e) {
            System.err.println("Blad");
            return null;
        }
    }

    public List<Activity> selectByDate(String date) {
        List<Activity> activities = new ArrayList<>();
        try {
            String query = "SELECT * FROM activity WHERE date_created = '" + date + "'";
            ResultSet result = stat.executeQuery(query);
            int id, quantity;
            String name, unit;
            while (result.next()) {
                id = result.getInt("ID");
                name = result.getString("name");
                unit = result.getString("unit");
                quantity = result.getInt("quantity");
                activities.add(new Activity(id, name, unit, quantity));
            }
        } catch (SQLException e) {
            System.err.println("Blad");
            return null;
        }

        System.out.println(activities.size());
        return activities;
    }

    public void updateActivity(Activity activity) {
        try {
            PreparedStatement prepStmt = conn.prepareStatement(
                    "UPDATE activity SET quantity = ? WHERE id = ?");
            prepStmt.setInt(1, activity.getQuantity());
            prepStmt.setInt(2, activity.getID());
            prepStmt.execute();
        } catch (SQLException e) {
            System.err.println("Blad");
            e.printStackTrace();
        }
    }
}
