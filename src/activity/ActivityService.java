package activity;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ActivityService {

    Repository repo;
    Activity act;

    public ActivityService(Repository repo) {
        this.repo = repo;
    }

    /**
     * @description metoda TWORZY NOWA aktywnosc - z nazwa, jednostka i automatyczna data ustawiona na dzien dzisiejszy i iloscia na 0
     * @param name
     * @param unit 
     */
    public void saveActivity(String name, String unit) {
        Activity activity = new Activity(name, unit, 0);
        this.repo.insertActivity(activity);
    }
    
    /**
     * @description metoda DODAJE AKTYWNOSC do juz istniejacej aktywnosci - wyciaga ja przez ID, nastepnie robi UPDATE
     * @param activityId
     * @param quantity 
     */
    public void addQuantity(int activityId, int quantity) throws SQLException
    {
        Activity activity = this.repo.selectById(activityId);
        activity.addQuantity(quantity);
        this.repo.updateActivity(activity);
    }
    
    public List<Activity> getTodayActivities()
    {
        return this.getActivitiesForDate(new Date());
    }
    
    public List<Activity> getActivitiesForDate(Date date)
    {
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        
        return this.repo.selectByDate(dateFormatter.format(date));
    }
}
