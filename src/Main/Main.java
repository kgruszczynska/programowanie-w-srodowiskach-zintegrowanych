package Main;

import activity.Activity;
import activity.Repository;
import java.sql.*;
import java.util.List;
import activity.ActivityService;

public class Main {

    public static void main(String[] args) throws SQLException {
        Repository repo = new Repository();
        ActivityService serAct = new ActivityService(repo);
        serAct.saveActivity("test2", "test2");
        System.out.println(repo.selectById(12).formatToString());
        
        repo.closeConnection();
    }

}
